package company;


import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleExtractor;

/* This code take homepage html code and extract sitetitle, description and keyword from meta tag, and maincontent from boilerpipe.
 * */
public class MetaDataDescription 
{
	static void callToMetaData(Document doc,Map<String,String> bean)
	{
		String siteTitle="";
		String description="";
		String keywords="";
		String mainContent="";
					
		//Site Title
		siteTitle=doc.title().replaceAll("[\n]+", "").replaceAll("[\\s]+", " ").trim();
		
		//Keywords and Description from Metadata Tag
		for(Element meta : doc.select("meta"))
		{
			if(meta.attr("name").toLowerCase().contains("keyword"))
			{
				keywords=(keywords.trim().equals("")? meta.attr("content"): (keywords+", "+meta.attr("content")) );
			}
			if(meta.attr("name").toLowerCase().contains("description"))
			{
				description+=" "+meta.attr("content");
			}
		}
		
		//Trim description
		description=description.replaceAll("\n", " ").replaceAll("[\\s]+", " ").trim();
		
		//Main content from boilerpipe.
		try 
		{
			mainContent = ArticleExtractor.INSTANCE.getText(doc.toString());
		} 
		catch (BoilerpipeProcessingException e) 
		{
			System.out.println(e.getClass().getName()+"\t"+e.getMessage());
			mainContent=doc.text();
		}
		
		//Trim Main Content
		mainContent.replaceAll("\n", " ").replaceAll("[\\s]+", " ").trim();				
		
		//If description is empty
		if(description.equals(""))
		{
			description=mainContent;
		}
		
		description=StringUtils.stripAccents(description);
		description=removeSpecialChars(description);
		
		//Trim Keywords	
		keywords=keywords.replaceAll("\n", " ").replaceAll("[\\s]+", " ").trim();
		
		//Set fields to Bean
		bean.put("siteTitle",siteTitle);
		bean.put("description",description);
		bean.put("keywords",keywords);
		bean.put("mainContent",mainContent);		
		
	}	
	
	// Remove special characters from description.
	static String removeSpecialChars(String description) {
		String refinedDescription = null;
		Pattern p = Pattern.compile("[^0-9A-Za-z-: ]+");
		Matcher m = p.matcher(description);
		if (m.find()) {
			refinedDescription = m.replaceAll(" "); // replace special char by space
			return refinedDescription;
		} else
			return description;
	}
}
