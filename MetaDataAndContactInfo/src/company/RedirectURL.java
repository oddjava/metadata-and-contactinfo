package company;

import java.io.IOException;

import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.Connection.Response;

/* This class will check whether url is redirecting or not */

public class RedirectURL 
{
	static String baseUrl;
	static int counter=0;
	
	//This method calls itself recursively to get final url location.
	static Response checkRedirectingURL(String url) 
	{
		String temp1="";
		String temp2="";
		Response response=null;
		try 
		{
			// Connect to url using JSoup and followRedirects(false), 30 sec timeout.
			response = Jsoup.connect(url).header("Accept-Language", "en").timeout(30000).followRedirects(false).execute();
			System.out.println(response.statusCode() + " : " + url);

			if(counter==8)  // If redirecting infinite then restricted to it to 8 time redirection.
			{
				counter=0;
				response=null;
				return response;
			}	
			if (response.hasHeader("location"))  //location header is set when url is redirected.
			{
				String redirectUrl = response.header("location");
				if(redirectUrl.trim().startsWith("/"))  //If redirected link starts with slash.
				{
					if(baseUrl.endsWith("/")) //If redirected link have only slash.
					{
						counter++;
						response=checkRedirectingURL(baseUrl.substring(0, baseUrl.length()-1)+redirectUrl);
					}
					else
					{
						counter++;
						response=checkRedirectingURL(baseUrl+redirectUrl);
					}
				}
				else if(redirectUrl.trim().startsWith("http")) //If redirected link starts with http.
				{
					//Check hostname of both links.
					temp1=new URL(baseUrl).getHost().replace("www.", "");
					temp2=new URL(redirectUrl).getHost().replace("www.", "");
							
					if(!temp1.equalsIgnoreCase(temp2))  // Different host means redirected.
					{
						System.out.println("Differnt Host=> "+temp1+"\t"+temp2);
						response=null;
						return response;
					}
					else
					{	
						counter++;
						response=checkRedirectingURL(redirectUrl);																	
					}
					
				}
				else
				{	counter++;
					response=checkRedirectingURL(baseUrl+"/"+redirectUrl);
				}			
			}
			return response;
		} 		
		catch (IOException e) 
		{
			return response;
		}
		catch (Exception e) 
		{
			return response;
		}
		
	}
	
	public static void main(String args[])
	{
		baseUrl="http://www.mountsilacrosse.org/";
		System.out.println(checkRedirectingURL("http://www.mountsilacrosse.org/").url());		
	}
}
