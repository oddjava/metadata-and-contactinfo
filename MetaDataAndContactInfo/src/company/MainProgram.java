package company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;

import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

/* This code is integration of MetaData and ContactInfo Codes.
 * This is single threaded code and maintains load average of CPU of VPS.
 * This code will get terminated when CPU load average exceed 1.5
 * This code is monitored by shell script. If this code is terminated by exceeding
   CPU load average then shell script will again execute this after 2 minutes. 
*/
public class MainProgram 
{
	// References related to Mongo Database.
	MongoClient mongoClient;
	DB company;
	DBCollection parallelWords;
	DBCollection parallelTitle;
	DBCollection parsedDataForVPS;
	
	//This String array will hold words regarding their categories.
	static String[] pricingWords,pricingTitle,eventWords,eventTitle,webinarWords,
	webinarTitle,newsWords,newsTitle,forumWords,forumTitle,contactWords,contactTitle,
	careerWords,careerTitle;
		
	// Constructor to Establish DB Connection 
	MainProgram(String ip)
	{
		mongoClient = new MongoClient(ip, 27017);
		company = mongoClient.getDB("sampleParser");
		parsedDataForVPS=company.getCollection("parsedData");
		parallelWords= company.getCollection("parallelWords");	
		parallelTitle= company.getCollection("parallelTitle");	
	}	
	
	/* This method will takes documents from "parsedDataForVPS" DB.
	   It will also takes parallel words and titles of each category from DB.
	   parallel words used to find links(contact link,career link) from company home page.
	*/
	boolean getMetaData(int startid,int endid) throws URISyntaxException
	{				
		//Get parallel words and titles per section
		
		pricingWords=getParallelWords("pricing").split(",");
		pricingTitle=getParallelTitles("pricing").split(",");
		
		eventWords=getParallelWords("event").split(",");
		eventTitle=getParallelTitles("event").split(",");
		
		webinarWords=getParallelWords("webinar").split(",");
		webinarTitle=getParallelTitles("webinar").split(",");
		
		newsWords=getParallelWords("news").split(",");
		newsTitle=getParallelTitles("news").split(",");
		
		forumWords=getParallelWords("forum").split(",");
		forumTitle=getParallelTitles("forum").split(",");
		
		contactWords=getParallelWords("contact").split(",");
		contactTitle=getParallelTitles("contact").split(",");
		
		careerWords=getParallelWords("career").split(",");
		careerTitle=getParallelTitles("career").split(",");
		
		DBCursor cursor=null;
		DBObject present;
		String link;
		String id;
		String redirctURL;
		Response response;
		String tempLink;
		double loads[];
		
		// Get documents from whose seller status is true and metadata status is false.
		cursor=parsedDataForVPS.find(new BasicDBObject("_id", new BasicDBObject("$lte", endid).append("$gte", startid)).append("metaDescriptionStatus","false").append("sellerStatus" , "true"));
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		
		System.out.println("Total Count="+cursor.size()+"\n");
			
		while(cursor.hasNext())
		{					
			loads=getLoadAvg();
			if(loads[0]<1.50)  // Check load Average < 1.5 
			{	
				link="";
				redirctURL="";
				tempLink="";
				
				present=cursor.next();
				link=present.get("link").toString();
				id=present.get("_id").toString();

				System.out.println("\n****************************************\n");

				try 
				{		
					// Send link to check whether it is redirecting or not
					RedirectURL.baseUrl=link.trim();
					System.out.println(id+"  Checking Redirect..."+(new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date())));
					response=RedirectURL.checkRedirectingURL(link.trim());

					//If response is null means infinite redirection or exception occurred or domain redirected.
					if(response==null)
					{
						parsedDataForVPS.update(new BasicDBObject("link",link), new BasicDBObject("$set",new BasicDBObject("sellerStatus","invalidDomain")));
						System.out.println("\tInvalid Domain");
					}
					else //response found mean got 200 status for link.
					{
						//Display input to checkRedirectingURL method and output returned.
						redirctURL=response.url().toString();
						System.out.println("Sent= "+link+"\nRecv= "+redirctURL);

						// Sometimes redirected url contains only slash
						if(redirctURL.trim().equals("/")) //validDomain
						{	
							parseDocument(response.parse(),link,parsedDataForVPS);
							System.err.println("Found /. Valid Domain");								
						}	
						else 
						{
							// Get hostname and replace www. from both link and redirected link
							tempLink=new URL(link).getHost().replace("www.", "");
							redirctURL=new URL(redirctURL).getHost().replace("www.", "");

							//If both links have different domain means link redirected to different domain.
							if(!tempLink.equalsIgnoreCase(redirctURL))
							{
								parsedDataForVPS.update(new BasicDBObject("link",link), new BasicDBObject("$set",new BasicDBObject("sellerStatus","redirectDomain")));
								System.err.println("\tRedirect Domain");
							}
							else // Valid Domain
							{	
								parseDocument(response.parse(),link,parsedDataForVPS);
								System.out.println("\tValid Domain");																	
							}
						}
					}

				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			else  // When cpu load average exceed 1.5 code will terminate.
			{
				System.out.println("So tired. I'm Shuting Nowwwwwww........"+(new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date())));
				System.exit(123);	// return status 123 to shell script indicating abnormal termination.			
			}
		}
		return true;
	}
	
	void parseDocument(Document doc,String link,DBCollection selectedCollection) throws IOException
	{
		// bean to hold all fields of metadata and contactinfo.
		Map<String,String> bean=new LinkedHashMap<String,String>();
		
		//call Metadata with Jsoup doc of company home url
		MetaDataDescription.callToMetaData(doc,bean);	
		
		//Call contactInfo with Jsoup doc of company home url
		new ContactInfo().callToContactInfo(doc,link,bean);
		
		System.out.println("Done MetaData\tDone ContactInfo");
		
		//Update status
		bean.put("metaDescriptionStatus","true");
		bean.put("contactInfoStatus","true");
		bean.put("sellerStatus","validDomain");
		
		//display Map
		/*for(Map.Entry<String, String> v : bean.entrySet() ) 
		{			   
			System.out.println(v.getKey()+" ="+v.getValue());
		}*/
			
		selectedCollection.update(new BasicDBObject("link",link),new BasicDBObject("$set",new BasicDBObject(bean)));	
		bean=null;
	}
	
	// fetching parallel Words according to type
	public String getParallelWords(String type) { 
		String parallelWord ="";
		DBObject present=null;
		DBCursor cursor = parallelWords.find(new BasicDBObject("type",type));
		if(cursor.hasNext())
		{
			present = cursor.next();
			parallelWord= present.get("parallelWords").toString();
			return parallelWord;
		}
		return parallelWord;
	}

	
	// fetching parallel Titles according to Type
	public String getParallelTitles(String type) 
	{ 
		String parallelTitles ="";
		DBObject present=null;
		DBCursor cursor = parallelTitle.find(new BasicDBObject("type",type));
		
		if(cursor.hasNext())
		{
			present = cursor.next();
			parallelTitles= present.get("parallelTitles").toString();
			return parallelTitles;
		}
		return parallelTitles;
	}
	
	public static void main(String[] args) throws URISyntaxException 
	{
		/* args[0] = IP Address
		 * args[1] = Start id
		 * args[2] = End id
		 * */
		MainProgram obj=new MainProgram(args[0]);
		obj.getMetaData(Integer.parseInt(args[1]),Integer.parseInt(args[2]));		
		System.out.println("Done");
		System.exit(0);	// return status 0 to shell script indicating normal termination.
	}
	
	// This method will get current load average of CPU using "uptime" command.
	double[] getLoadAvg()
	{
		StringBuffer output = new StringBuffer();
		double loads[]=new double[3];
        Process p;
        try {
                p = Runtime.getRuntime().exec("uptime"); // execute "uptime" command on linux only.
                p.waitFor();
                BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

                String line = "";
                while ((line = reader.readLine())!= null) {
                     output.append(line + "\n");  //Output of command
                }

        } catch (Exception e) {
                e.printStackTrace();
        }
        
        
        String str=output.toString();
        str=str.split("load average[s:][: ]")[1];  //To get only load avg from command output
        System.out.println("Load Average= "+str);
        String allValues[]=str.split(",");
        loads[0]=Double.parseDouble(allValues[0].trim()); //last one Min
        loads[1]=Double.parseDouble(allValues[1].trim()); //last five Min
        loads[2]=Double.parseDouble(allValues[2].trim()); //last fifteen Min
       // System.out.println(lastOneMin+"**"+lastFiveMin+"**"+lastFifteenMin+"**");
        return loads;

	}

}


