package company;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.KeepEverythingExtractor;

public class ContactInfo 
{		
	// Variables to hold DB fields.
	String twitterLink,facebookLink,linkedinLink,googleLink,instagramLink,pinterestLink;
	
	String webinarLink,eventLink,forumLink,careerLink,pricingLink,newsLink,contactLink;
	
	String fbRegex,linkedinRegex,twitterRegex,googleRegex,instaRegex,pinterestRegex;
				
	String emailId,phone;
	
	String companyName = null;
	String textProcess="";
		
	// Set values of Regex.
	ContactInfo()
	{
		fbRegex ="((http|https)://)?(www[.])?(in.)?facebook.com/.+";
		linkedinRegex = "((http|https)://)?(www[.])?(in.)?linkedin.com/.+";
		twitterRegex ="((http|https)://)?(www[.])?(in.)?twitter.com/.+";
		googleRegex = "((http|https)://)?(www[.])?(plus.)?google.com/.+";
		instaRegex = "((http|https)://)?(www[.])?(in.)?instagram.com/.+";
		pinterestRegex ="((http|https)://)?(www[.])?(in.)?pinterest.com/.+";
		emailId="";
		phone="";
		careerLink=""; pricingLink=""; eventLink=""; webinarLink=""; 
		newsLink=""; forumLink=""; contactLink="";
		twitterLink="";facebookLink="";linkedinLink="";googleLink="";instagramLink="";pinterestLink="";
	}
	
	// Take Bean object from mainProgram.
	void callToContactInfo(Document doc,String link,Map<String,String> bean) 
	{
		
		ExecutorService executor;
		Future<?> future;
		
		// Get all Social Links using regex from homepage 
		facebookLink=getSocialLink(doc,fbRegex).trim();
		twitterLink=getSocialLink(doc,twitterRegex).trim();
		googleLink=getSocialLink(doc,googleRegex).trim();
		linkedinLink=getSocialLink(doc,linkedinRegex).trim();
		instagramLink=getSocialLink(doc,instaRegex).trim();
		pinterestLink=getSocialLink(doc,pinterestRegex).trim();
				
		try
		{
			/* For Some links specially news links, this Code gets halt. 
			 * To avoid halting, we have given 10 sec time for executing getTypeLink method.
			 * If method doesn't return in 10 sec then method execution is terminated abnormally and program will continue. 
			 * getTypeLink() is called using single thread.
			 * */
			
			//Career
			executor = Executors.newFixedThreadPool(1);   //set max limit to a task
			future = executor.submit(new Runnable() 
			{	@Override
				public void run() 
				{
					careerLink=getTypeLink(doc, MainProgram.careerWords, MainProgram.careerTitle).trim(); 
				}
			});
			executor.shutdown();            //        <-- reject all further submissions
			try 
			{
				future.get(10, TimeUnit.SECONDS);  //     <-- wait 8 seconds to finish
			} 
			catch (Exception e) 
			{    //     <-- possible error cases
				System.out.println("ContactInfo interrupted");
			}
			
			//Pricing
			executor = Executors.newFixedThreadPool(1);   //set max limit to a task
			future = executor.submit(new Runnable() 
			{	@Override
				public void run() 
				{
					pricingLink=getTypeLink(doc, MainProgram.pricingWords, MainProgram.pricingTitle).trim(); 
				}
			});
			executor.shutdown();            //        <-- reject all further submissions
			try 
			{
				future.get(10, TimeUnit.SECONDS);  //     <-- wait 8 seconds to finish
			} 
			catch (Exception e) 
			{    //     <-- possible error cases
				System.out.println("ContactInfo interrupted");
			}
			
			
			//Event
			executor = Executors.newFixedThreadPool(1);   //set max limit to a task
			future = executor.submit(new Runnable() 
			{	@Override
				public void run() 
				{
					eventLink=getTypeLink(doc,MainProgram.eventWords, MainProgram.eventTitle).trim(); 
				}
			});
			executor.shutdown();            //        <-- reject all further submissions
			try 
			{
				future.get(10, TimeUnit.SECONDS);  //     <-- wait 8 seconds to finish
			} 
			catch (Exception e) 
			{    //     <-- possible error cases
				System.out.println("ContactInfo interrupted");
			}
			
			//Webinar
			executor = Executors.newFixedThreadPool(1);   //set max limit to a task
			future = executor.submit(new Runnable() 
			{	@Override
				public void run() 
				{
					webinarLink=getTypeLink(doc,MainProgram.webinarWords, MainProgram.webinarTitle).trim(); 
				}
			});
			executor.shutdown();            //        <-- reject all further submissions
			try 
			{
				future.get(10, TimeUnit.SECONDS);  //     <-- wait 8 seconds to finish
			} 
			catch (Exception e) 
			{    //     <-- possible error cases
				System.out.println("ContactInfo interrupted");
			}
			
			
			//News
			executor = Executors.newFixedThreadPool(1);   //set max limit to a task
			future = executor.submit(new Runnable() 
			{	@Override
				public void run() 
				{
					newsLink=getTypeLink(doc, MainProgram.newsWords, MainProgram.newsTitle).trim(); 
				}
			});
			executor.shutdown();            //        <-- reject all further submissions
			try 
			{
				future.get(10, TimeUnit.SECONDS);  //     <-- wait 8 seconds to finish
			} 
			catch (Exception e) 
			{    //     <-- possible error cases
				System.out.println("ContactInfo interrupted");
			}
			
			
			//Forum
			executor = Executors.newFixedThreadPool(1);   //set max limit to a task
			future = executor.submit(new Runnable() 
			{	@Override
				public void run() 
				{
					forumLink=getTypeLink(doc,MainProgram.forumWords, MainProgram.forumTitle).trim(); 
				}
			});
			executor.shutdown();            //        <-- reject all further submissions
			try 
			{
				future.get(10, TimeUnit.SECONDS);  //     <-- wait 8 seconds to finish
			} 
			catch (Exception e) 
			{    //     <-- possible error cases
				System.out.println("ContactInfo interrupted");
			}
			
			
			//Contact
			executor = Executors.newFixedThreadPool(1);   //set max limit to a task
			future = executor.submit(new Runnable() 
			{	@Override
				public void run() 
				{
					contactLink= getTypeLink(doc,MainProgram.contactWords, MainProgram.contactTitle).trim(); 
				}
			});
			executor.shutdown();            //        <-- reject all further submissions
			try 
			{
				future.get(10, TimeUnit.SECONDS);  //     <-- wait 8 seconds to finish
			} 
			catch (Exception e) 
			{    //     <-- possible error cases
				System.out.println("ContactInfo interrupted");
			}			
		}		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		// Get company name from website url to check whether it is present in company email.
		companyName=getCompany(link);		
		System.out.println("companyName="+companyName+"\t"+contactLink);
		
		// For Contact page Html code, Extract emailID and Phone.
		if(!contactLink.equals("")) 
		{
			Document document = null;
			try 
			{
				document = Jsoup.connect(contactLink).header("Accept-Language", "en").timeout(30000).followRedirects(false).get();
				
				//Try Again for social links if not found on homepage.
				if(facebookLink.equals(""))
				{
					facebookLink=getSocialLink(document,fbRegex);
				}
				
				if(twitterLink.equals(""))
				{
					twitterLink=getSocialLink(document,twitterRegex);
				}
				
				if(googleLink.equals(""))
				{
					googleLink=getSocialLink(document,googleRegex);
				}
				
				if(linkedinLink.equals(""))
				{
					linkedinLink=getSocialLink(document,linkedinRegex);
				}
				
				if(instagramLink.equals(""))
				{
					instagramLink=getSocialLink(document,instaRegex);
				}
				
				if(pinterestLink.equals(""))
				{
					pinterestLink=getSocialLink(document,pinterestRegex);
				}
				
				//For EmailID and Phone, Get proper content using boilerpipe.
				try 
				{
					textProcess = KeepEverythingExtractor.INSTANCE.getText(document.toString());
					textProcess=textProcess.replace("[at]", "@").replace("(at)", "@").replace(" @ ", "@");
					emailId=getEmailIdContact(textProcess, companyName).trim();
					phone=getPhone(textProcess).trim();
				}
				catch (BoilerpipeProcessingException e) 
				{
					System.out.println(e.getClass().getName()+"\t"+e.getMessage());
				}
			} 
			catch (IOException e1) 
			{
				//System.out.println(id+"  (In ContactInfo)= "+e1.getClass().getName()+" Exception for link "+contactLink);
			}						
		}		
				
		//If email and phone not found on contact page then find it on homepage.
		if(emailId.equals("") || phone.equals(""))     
		{
			try 
			{
				//For EmailID and Phone, Get proper content using boilerpipe.
				
				textProcess = KeepEverythingExtractor.INSTANCE.getText(doc.toString());
				textProcess=textProcess.replace("[at]", "@").replace("(at)", "@").replace(" @ ", "@");
				if(emailId.equals(""))
				{
					emailId=getEmailId(textProcess, companyName);
				}		
				if(phone.equals(""))
				{
					phone=getPhone(textProcess);
				}
			} 
			catch (BoilerpipeProcessingException e) 
			{
				System.out.println(e.getClass().getName()+"\t"+e.getMessage());
			}			
		}
	
		//Set values to Bean
		bean.put("facebookLink",facebookLink);
		bean.put("twitterLink",twitterLink);
		bean.put("googleLink",googleLink);
		bean.put("linkedinLink",linkedinLink);
		bean.put("instagramLink",instagramLink);
		bean.put("pinterestLink",pinterestLink);
		bean.put("careerLink",careerLink);
		bean.put("pricingLink",pricingLink);
		bean.put("eventLink",eventLink);
		bean.put("webinarLink",webinarLink);
		bean.put("newsLink",newsLink);
		bean.put("forumLink",forumLink);
		bean.put("contactLink",contactLink);
		bean.put("companyEmail",emailId);
		bean.put("companyNumber",phone);
		
	}
		
	//Get Social Links from Homepage using regex.
	public String getSocialLink(Document doc,String regex)
	{
		String socialLink ="";
		try
		{
			Elements links = doc.select("a[href]");
			for(Element link:links)
			{	
				if(link.absUrl("abs:href").toLowerCase().matches(regex))
				{
					if(!link.absUrl("abs:href").toLowerCase().contains("/share"))
					{
						socialLink = link.attr("abs:href"); 
						break;
					}
				}
			}
		}
		catch(Exception e)
		{ 
			e.printStackTrace();
		}
		
		return socialLink;
	}
	
	//Get Typelink news,event,contact,career
	public String getTypeLink(Document doc, String[] words, String[] titles)
	{
		String typeLink = "";
		Elements links = doc.select("a[href]");
		String 	linkTest;
		String text;
		boolean flag = false;
		
		for(Element link:links)
		{			
			linkTest="";
			text="";
			
			linkTest = link.absUrl("abs:href").toLowerCase();
			text=link.text().toLowerCase();
			
			for(String word : words)  // Check word in links
			{					
				if(linkTest.contains(word)) 
				{	
					for(String title : titles) //check words in link title.
					{	
						if(text.matches("(\\D*\\s)*"+title+"(\\s\\D*)*"))
						{
							// If link found then terminate method. 
							typeLink = link.attr("abs:href");  //add the  Typelink here
							flag = true;
							break;
						}						
					}					
				}
				if(flag)
				{
					break;
				}	
			}
			if(flag)
			{
				break;
			}
		}

		return typeLink;
	}
	
	// Get company name from link.
	public String getCompany(String url) 
	{
		String companyName="";
		String host;
		try 
		{
			host = new URL(url).getHost().replace("www.", "");
			companyName = host.substring(0, host.indexOf(".")).toLowerCase();
		} 
		catch (MalformedURLException e) 
		{
			System.out.println(e.getClass().getName()+"\t"+e.getMessage()+"\t"+url);
		}
		
		return companyName;
	}
	
	// Checks email id on Home page
	public String getEmailId(String textProcess,String companyName)
	{
		String emailId="";
		Set<String> email = new HashSet<String>();
		// Email Id regex.
		Matcher m = Pattern.compile("[a-zA-Z0-9_.+-]+\\s?@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(textProcess);
		while (m.find()) 
		{
			if(m.group().toLowerCase().contains(companyName))
			{
				email.add(m.group()); 
			}	
		}

		if(!email.isEmpty()) //if emailId is present
		{
			for(String e1:email)
			{
				if(!e1.equals("")) // checking emailID if it null
				{
					emailId+=e1+",";
					//System.out.println("inside"+emailId);
				}
			}
			if(!emailId.isEmpty())
			{
				emailId = emailId.substring(0,emailId.length()-1); //Remove the last comma(,)
			}
		}
		//System.out.println("!!!!!!!!!!!!!!!!!"+emailId);
		return emailId;
	}

	// Checks email id on Contact us page
	public String getEmailIdContact(String textProcess,String companyName)
	{
		String emailId="";
		Set<String> email = new HashSet<String>();
		Matcher m = Pattern.compile("[a-zA-Z0-9_.+-]+\\s?@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(textProcess);
		while (m.find()) 
		{			
			email.add(m.group());					
		}

		if(!email.isEmpty()) //if emailId is present
		{
			for(String e1:email)
			{
				if(!e1.equals("")) // checking emailID if it null
				{
					emailId+=e1+",";
					//System.out.println("inside"+emailId);
				}
			}
			if(!emailId.isEmpty())
			{
				emailId = emailId.substring(0,emailId.length()-1); //Remove the last comma(,)
			}
		}
		//System.out.println("!!!!!!!!!!!!!!!!!"+emailId);
		return emailId;
	}
	
	public String getPhone(String textProcess)
	{
		String phone="";
		Set<String> phn = new HashSet<String>();
		// Regex for phone
		Matcher m1 = Pattern.compile("\\D*:?\\+?[ 0-9. ()-]{10,25}").matcher(textProcess);
		while (m1.find()) 
		{
			String num=m1.group().replaceAll("[^0-9]","").trim();// Remove the starting and ending spaces only take phone number others are remove like(,.;:'"?/()*+-_%$#!=))
			if(num.length() < 25 && num.length() >= 7) //constraints for phone number
			{
				phn.add(num);
			}
		}
		if(!phn.isEmpty()) //if the phone number is present
		{
			for(String p:phn)
			{
				phone+=p+","; 
			}
			phone= phone.substring(0, phone.length()-1);  //Remove the last comma(,)

		}	
		return phone;
	}
	
	public static void main(String[] args) throws IOException
	{
		//new ContactInfo().callToContactInfo(Jsoup.connect("http://cosas.com.ec/contacto/").get(), "http://cosas.com.ec/contacto/");
	}
	
}
